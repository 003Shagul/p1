//1. Find all Web Developers

function findAllWebDevelopers(data) {
  let allWebDevelopers = data.filter((eachData) =>
    eachData.job.startsWith("Web Developer")
  );
  return allWebDevelopers;
}

module.exports = findAllWebDevelopers;


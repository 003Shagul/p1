// 6. Find the average salary of based on country using only HOF method

// {
//   id: 1,
//   first_name: "Gregg",
//   last_name: "Lacey",
//   job: "Web Developer III",
//   salary: "$3.62",
//   location: "Malta",
// },

function findSumOfSalaryAverageByCountry(data, convertSalaryFn) {
  let convertedSalary = convertSalaryFn(data);
  let sumOfSalaryAverageByCountry = convertedSalary.reduce((acc, start) => {
    let location = start.location;
    let salary = start.corrected_salary;

    if (location in acc) {
      acc[location]["salary"] += salary;
      acc[location]["count"] += 1;
    } else {
      acc[location] = {};
      acc[location]["salary"] = salary;
      acc[location]["count"] = 1;
    }
    acc[location]["average"] = parseFloat(
      acc[location]["salary"] / acc[location]["count"]
    );

    return acc;
  }, {});

  const countries = Object.entries(sumOfSalaryAverageByCountry);
  console.log(countries);

  const avgSalariesPerCountry = countries.reduce((avgSalaries, country) => {
    let location = country[0];
    let obj = country[1];

    avgSalaries[location] = Number(obj["average"].toFixed(1));

    return avgSalaries;
  }, {});

  return avgSalariesPerCountry;
}

module.exports = findSumOfSalaryAverageByCountry;


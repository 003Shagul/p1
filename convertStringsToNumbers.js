// 2.Convert all the salary values into proper numbers instead of strings

// {
//   id: 1,
//   first_name: "Gregg",
//   last_name: "Lacey",
//   job: "Web Developer III",
//   salary: "$3.62",
//   location: "Malta",
// },

function convertStringsToNumbers(data) {
  let convertedValue = data.map((eachData) => {
    eachData.salary = parseFloat(eachData.salary.replace("$", ""));
    return eachData;
  });
  return convertedValue;
}

module.exports = convertStringsToNumbers;


//3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

// {
//   id: 1,
//   first_name: "Gregg",
//   last_name: "Lacey",
//   job: "Web Developer III",
//   salary: "$3.62",
//   location: "Malta",
// },

function correctedSalary(data) {
  let correctedSalaryFactor = data.map((eachData) => {
    eachData["corrected_salary"] =
      parseFloat(eachData.salary.replace("$", "")) * 10000;
    return eachData;
  });
  return correctedSalaryFactor;
}

module.exports = correctedSalary;


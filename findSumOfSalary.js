//4. Find the sum of all salaries

// {
//   id: 1,
//   first_name: "Gregg",
//   last_name: "Lacey",
//   job: "Web Developer III",
//   salary: "$3.62",
//   location: "Malta",
// },

function findSumOfSalary(data, cb) {
  let convertedSalaryData = cb(data);

  let sumOfSalary = convertedSalaryData.reduce((acc, start) => {
    acc += start["corrected_salary"];
    return acc;
  }, 0);
  return sumOfSalary;
}

module.exports = findSumOfSalary;


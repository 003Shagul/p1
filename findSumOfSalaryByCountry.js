// 5. Find the sum of all salaries based on country using only HOF method

// {
//   id: 1,
//   first_name: "Gregg",
//   last_name: "Lacey",
//   job: "Web Developer III",
//   salary: "$3.62",
//   location: "Malta",
// },

function findSumOfSalaryByCountry(data, convertSalaryFn) {
  let convertedSalary = convertSalaryFn(data);
  let sumOfSalaryByCountry = convertedSalary.reduce((acc, start) => {
    if (start.location in acc) {
      acc[start.location] += start.corrected_salary;
    } else {
      acc[start.location] = start.corrected_salary;
    }

    return acc;
  }, {});
  return sumOfSalaryByCountry;
}

module.exports = findSumOfSalaryByCountry;

